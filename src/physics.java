/**
 * Created with IntelliJ IDEA.
 * User: Michal
 * Date: 3/17/13
 * Time: 10:35 PM
 */

import static org.lwjgl.opengl.Display.create;
import static org.lwjgl.opengl.Display.destroy;
import static org.lwjgl.opengl.Display.isCloseRequested;
import static org.lwjgl.opengl.Display.setDisplayMode;
import static org.lwjgl.opengl.Display.update;
import static org.lwjgl.opengl.GL11.*;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.DisplayMode;

public class physics {

    static final private double radius = 0.035;

    private static class Circle {
        double x, y, radius;
        double velX;
        double velY;

        public Circle(double x, double y, double r) {
            this.x = x;
            this.y = y;
            this.radius = r;
            this.velX = 0;
            this.velY = 0;
        }

        public void update() {
            this.x += velX;
            this.y += velY;
        }

        public void display() {
            glBegin(GL_TRIANGLE_FAN);
            glVertex2d(x, y);
            for (int i = 0; i <= 30; ++i) {
                double theta = i*2.0*Math.PI/10.0;
                glVertex2d(x+radius*Math.cos(theta), y+radius*Math.sin(theta));
            }
            glEnd();
        }

    }
    public static void collisionBoundary(Circle p){
        //get distance to the center of the circle
        double newX = p.x + p.velX;
        double newY = p.y + p.velY;
        double distance = Math.sqrt(Math.pow(newX,2)+Math.pow(newY,2));
        if (distance > 1) {
            double changeX = newX/distance;
            double changeY = newY/distance;
            p.velX += 2* (changeX - newX);
            p.velY += 2* (changeY - newY);
        }
    }
    static int width = 720;
    static int height = 720;
    static boolean fullscreen = false;
    public static void main(final String[] args) {
        Circle[] circles = new Circle[600];
        for (int i = 0; i < circles.length; ++i) {
            double x = Math.random()*2 - 1;
            double y = Math.random()*2 - 1;
            double distance = Math.sqrt(Math.pow(x,2)+Math.pow(y,2));
            while (distance > 1) {
                x = Math.random()*2 - 1;
                y = Math.random()*2 - 1;
                distance = Math.sqrt(Math.pow(x,2)+Math.pow(y,2));
            }
            circles[i] = new Circle(x,y, radius);
            circles[i].velX = (Math.random()*2 - 1)/50000;
            circles[i].velY = (Math.random()*2 - 1)/50000;
        }

        try {
            setDisplayMode(new DisplayMode(width, height));

            create();
            while (!isCloseRequested()) {
                //render
                glClear(GL_DEPTH_BUFFER_BIT|GL_COLOR_BUFFER_BIT);
                glColor3d(0,0.2,1);
                 //repel
                for (Circle p : circles) {
                    for (Circle p2 : circles) {
                        if (p != p2){
                            double difX = (p.x + p.velX)- (p2.x + p2.velX); //Predicted positions
                            double difY = (p.y + p.velY)- (p2.y + p2.velY);
                            double distance = Math.sqrt(Math.pow(difX,2)+Math.pow(difY,2));
                            if (distance < radius*2) {       //collision detection
                                difX /= distance;
                                difY /= distance;
                                double force = (radius*2-distance)/100; //Lava is about 0.055
                                p.velX+=force*difX;
                                p.velY+=force*difY;
                                p2.velX-=force*difX;
                                p2.velY-=force*difY;
                            }
                        }
                    }
                    p.velY -= 0.0002; //gravity!
                    //air resistance
                    p.velX *= 0.999;
                    p.velY *= 0.999;
                }
                for (Circle c : circles) {
                    c.display();
                }
                glColor3d(0,0,1);
                for (Circle c : circles) {
                    double tempRadius = radius*1.15;
                    c.radius = tempRadius;
                    c.display();
                    c.radius = radius*1.4;
                }
                for (Circle c : circles) {
                    collisionBoundary(c);
                    c.update();
                }
                update();
            }
            try {
                Thread.sleep(50); // sleep to use less system resources
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        } catch (LWJGLException e) {
            e.printStackTrace();
        }
        destroy();
    }
}
